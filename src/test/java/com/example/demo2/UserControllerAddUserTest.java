package com.example.demo2;

import com.example.demo2.security.TestCredentialsRepository;
import com.example.demo2.user.credentials.UserCredentialsRepository;
import com.example.demo2.user.crud.UserConverter;
import com.example.demo2.user.crud.UserService;
import com.example.demo2.user.crud.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


import javax.sql.DataSource;

import static com.example.demo2.constants.UserDto__TestConstants.USERS_VALID_USER_DTO;
import static com.example.demo2.constants.UserEntity__TestConstants.USER_VALID_ENTITY;
import static com.example.demo2.constants.UserMetadata__TestConstants.USERS_URL;
import static com.example.demo2.constants.UserMetadata__TestConstants.USERS_VALID_USER_ID;
import static com.example.demo2.security.TestCredentialsRepository.PASSWORD;
import static com.example.demo2.security.TestCredentialsRepository.USER_USERNAME;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@WebMvcTest(UserController.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(MockitoExtension.class)
class UserControllerAddUserTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserService userService;

    @MockBean
    UserConverter userConverter;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserCredentialsRepository userCredentialsRepository;

    @Autowired
    DataSource dataSource;

    @BeforeEach
    void init() {
        TestCredentialsRepository testCredentialsRepository = new TestCredentialsRepository(dataSource);
        testCredentialsRepository.createUserPrincipal();
    }

    @AfterEach
    void clean() {
        TestCredentialsRepository testCredentialsRepository = new TestCredentialsRepository(dataSource);
        testCredentialsRepository.removeAllPrincipals();
    }

    @DisplayName("after saving user entity returns its id and status code 201")
    @Test
    @WithMockUser(
            username = USER_USERNAME,
            password = PASSWORD,
            roles = "USER"
    )
    void shouldAddUser() throws Exception {
        String json = objectMapper.writeValueAsString(
                USERS_VALID_USER_DTO
        );
        when(userConverter.mapToEntity(USERS_VALID_USER_DTO)).thenReturn(
                USER_VALID_ENTITY
        );
        when(userService.addUser(USER_VALID_ENTITY)).thenReturn(
                USERS_VALID_USER_ID
        );

        mockMvc
                .perform(post(USERS_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                )
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().string(String.valueOf(USERS_VALID_USER_ID)));
    }

}

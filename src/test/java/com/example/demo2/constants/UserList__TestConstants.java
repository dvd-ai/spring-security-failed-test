package com.example.demo2.constants;


import com.example.demo2.user.crud.User;
import com.example.demo2.user.crud.UserDto;

import java.util.List;

import static com.example.demo2.constants.UserEntity__TestConstants.USER_VALID_ENTITY;
import static com.example.demo2.constants.UserMetadata__TestConstants.*;

public class UserList__TestConstants {

    public static final List<User> USER_USER_VALID_ENTITY_LIST = List.of(
            new User(
                    USERS_VALID_USER_ID, USER_VALID_ENTITY.getFirstName(),
                    USER_VALID_ENTITY.getLastName(), USER_VALID_ENTITY.getPhoneNumber(),
                    USER_VALID_ENTITY.getEmail(), USER_VALID_ENTITY.getRole(), USERS_REGISTRATION_DATE
            ),
            new User(
                    USERS_VALID_USER_ID + 1, USER_VALID_ENTITY.getFirstName(),
                    USER_VALID_ENTITY.getLastName(), USER_VALID_ENTITY.getPhoneNumber(),
                    USER_VALID_ENTITY.getEmail(), USER_VALID_ENTITY.getRole(), USERS_REGISTRATION_DATE
            ),
            new User(
                    USERS_VALID_USER_ID + 2, USER_VALID_ENTITY.getFirstName(),
                    USER_VALID_ENTITY.getLastName(), USER_VALID_ENTITY.getPhoneNumber(),
                    USER_VALID_ENTITY.getEmail(), USER_VALID_ENTITY.getRole(), USERS_REGISTRATION_DATE
            )
    );

    public static final List<UserDto> USERS_USER_VALID_DTO_LIST = List.of(
            new UserDto(
                    USERS_VALID_USER_ID, USER_USER_VALID_ENTITY_LIST.get(0).getFirstName(),
                    USER_USER_VALID_ENTITY_LIST.get(0).getLastName(), USER_USER_VALID_ENTITY_LIST.get(0).getPhoneNumber(),
                    USER_USER_VALID_ENTITY_LIST.get(0).getEmail(), USERS_ROLE, USERS_REGISTRATION_DATE
            ),
            new UserDto(
                    USERS_VALID_USER_ID + 1, USER_USER_VALID_ENTITY_LIST.get(1).getFirstName(),
                    USER_USER_VALID_ENTITY_LIST.get(1).getLastName(), USER_USER_VALID_ENTITY_LIST.get(1).getPhoneNumber(),
                    USER_USER_VALID_ENTITY_LIST.get(1).getEmail(), USERS_ROLE, USERS_REGISTRATION_DATE
            ),
            new UserDto(
                    USERS_VALID_USER_ID + 2, USER_USER_VALID_ENTITY_LIST.get(2).getFirstName(),
                    USER_USER_VALID_ENTITY_LIST.get(2).getLastName(), USER_USER_VALID_ENTITY_LIST.get(2).getPhoneNumber(),
                    USER_USER_VALID_ENTITY_LIST.get(2).getEmail(), USERS_ROLE, USERS_REGISTRATION_DATE
            )
    );
}

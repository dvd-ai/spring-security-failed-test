package com.example.demo2;

import com.example.demo2.user.credentials.UserCredentialsRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static com.example.demo2.constants.UserMetadata__TestConstants.USERS_URL;
import static com.example.demo2.security.TestCredentialsRepository.USER_USERNAME;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserControllerIT {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Autowired
    UserCredentialsRepository userCredentialsRepository;


    @Test
    void getUsers() {
        System.out.println(userCredentialsRepository.credentialsExistByUsername(USER_USERNAME));

        ResponseEntity<String> response = testRestTemplate.
                getForEntity(USERS_URL, String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

}

package com.example.demo2.error;

import java.util.List;

public record ErrorDto(List<String> errors) {
}

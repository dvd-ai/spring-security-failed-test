package com.example.demo2.exception;

import java.util.List;

public class DbUniqueConstraintsException extends RuntimeException {
    private final List<String> messages;

    public DbUniqueConstraintsException(List<String> messages) {
        this.messages = messages;
    }

    public List<String> getMessages() {
        return messages;
    }
}

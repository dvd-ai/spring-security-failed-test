package com.example.demo2.security;

import com.example.demo2.user.credentials.UserCredentials;
import com.example.demo2.user.credentials.UserCredentialsRepository;
import com.example.demo2.user.crud.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DomainUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;
    private final UserCredentialsRepository userCredentialsRepository;

    public DomainUserDetailsService(UserRepository userRepository, UserCredentialsRepository userCredentialsRepository) {
        this.userRepository = userRepository;
        this.userCredentialsRepository = userCredentialsRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final Optional<UserCredentials> customer = userCredentialsRepository.findByUsername(username);
        if (customer.isEmpty()) {
            throw new UsernameNotFoundException(username);
        }
        UserCredentials userCredentials = customer.get();
        com.example.demo2.user.crud.User foundUser = userRepository.findUserById(userCredentials.getUserId()).get();
        return User.withUsername(userCredentials.getUsername()).password(userCredentials.getPassword()).authorities(foundUser.getRole()).build();
    }

}

INSERT INTO users (first_name, last_name, phone_number,
                   email, role, registration_date)
VALUES ('denis', 'ivaylo', '+38092334455', 'denis@gmail.com', 'barber', '2021-03-02T14:21:23'),
       ('kate', 'bishop', '+38092334411', 'kate@gmail.com', 'barber', '2021-06-02T14:21:23'),
       ('tor', 'odinson', '+38093334422', 'tor@gmail.com', 'barber', '2021-06-02T18:00:00'),
       ('loki', 'odinson', '+38090334499', 'loki@gmail.com', 'barber', '2021-10-02T10:00:00'),
       ('groot', 'tree', '+12098334422', 'iamgroot@gmail.com', 'admin', '2022-03-23T13:34:30'),
       ('frozen', 'girl', '+1111111111', 'frozen@gmail.com', 'user', '2019-08-20T17:34:22'),
       ('Bob', 'Thompson', '+1341177117', 'bob@gmail.com', 'user', '2010-08-20T17:00:01')
;

INSERT INTO user_credentials(user_id, username, password, enabled)
VALUES (1, 'divaylo', '$2a$12$DK9huzm6rDGtn1A83.uvvOV1GIWWfqJxlmmC5l/AOlRkW6u/2hLFy', true),
       (2, 'kbishop', '$2a$12$VmAPVrCsrWgCTVwF.oN8fezyi2R5zXwFU5V6bjzY./mU0HhR.UUa2', true),
       (3, 'todinson', '$2a$12$.Jj.71Zm.NKVX6Aj9Brpk.0/YZ8c3a0r9kvN4PFKvawozs6ML1lyG', true),
       (4, 'lodinson', '$2a$12$h2EXBpf4N0AAL5uoyU48OeUjQL7//mRQrC1c63qGe8BeDeYW22dLO', true),
       (5, 'gtree', '$2a$12$RIBs.wZikWcpuZTRAk73/eCCD5cmMqYC1EvOWk986tbo37ej0r5aC', true),
       (6, 'fgirl', '$2a$12$qGf/FaWTc5U5Id6oHF5L1Ou8Z1rJqqd/8imzA7U9YeG1A8wRxX8M.', true),
       (7, 'bthompson', '$2a$12$WqKEWd3VkKTPOZpqONP6uOyiQDF7Oyv7NN6Z9wsQQxqe5zwM9fa/a', true)
;